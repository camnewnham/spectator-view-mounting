# Spectator View Mount #

This design is based on [Microsoft's Spectator View mount](https://github.com/Microsoft/HoloLensCompanionKit/tree/master/SpectatorView), released under the MIT License.

The design and tolerances are designed to echo the CNC files.

The design is intended to enable hot-swapping of devices, and as such removal of the headband is not required. 

![Axo.jpg](https://bitbucket.org/repo/AgX69KR/images/488972260-Axo.jpg)![Axo_Exploded.jpg](https://bitbucket.org/repo/AgX69KR/images/1735744818-Axo_Exploded.jpg)

### Fixings ###

* (4) M3x16mm screws
* (2) M3x8mm screws
* (6) M5x20mm screws
* (6) M3 nut 
* (6) M5 nut
* (1) Hotshoe fastener
* (1) 3/16" nut

Locknuts should be used in place of regular nuts for permanent installation.

### 3D Printing ###

All files are designed to be printed via FFF/FDM without support, except for the camera mounting bracket.

All files easily fit individually on a 200x200mm bed.

![Printing.jpg](https://bitbucket.org/repo/AgX69KR/images/2735189144-Printing.jpg)

### Assembly ###

1. Fix hotshoe fastener to camera mounting bracket using 3/16" nut.
2. Fix left and right brackets to camera mounting bracket using M5x20mm screws.
3. Insert Hololens
4. (Optional) Fix front pad or front pad with alignment pins to front bracket using M3x8mm screws.
5. (Optional) Fix front and rear mounting brackets to left and right brackets using M3x16mm screws.

![Rear.jpg](https://bitbucket.org/repo/AgX69KR/images/1444272324-Rear.jpg)